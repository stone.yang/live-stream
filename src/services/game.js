import config from "config";

export default class GameService {
    static getGamesAPI() {
        return fetch(config.GAMES_API, {
            method: 'GET',
            headers: {
                'Platform':'Web_1.0'
            }
        }).then(function(res) {
            return res.json();
        }).then(function(data) {
            return data;
        })
    }
    static async getGames() {
        const data = await this.getGamesAPI();
        const statusList = [2, 3, 4];
        // 1未開賽 2上半場 3中場 4下半場 8已完賽
        // 直播要 2、3、4
        if (config.DEV_ENV) {
            // data.Payload = data.Payload.slice(0, 10);
            // data.Payload[0].liveStreamingHttps = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8";
            // data.Payload[1].liveStreamingHttps = "https://play.ykjhlmy.cn/arthur/sd-1-3556339.m3u8";
            // data.Payload[2].liveStreamingHttps = "https://example.com/test-hls/index.m3u8";
        } else{
            data.Payload = data.Payload.filter(m => (statusList.indexOf(m.status) > -1));
        }
        return data.Payload;
    }
}