import { connect } from 'react-redux'
import Login from 'components/login'
import { user_get, conn_get } from 'action'
import SessionService from "services/session";
import config from "config";

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    checkLogin: function() {
      // todo 驗證
      const token = SessionService.getToken();
      if (token != null){
        ownProps.history.push('/');
      }
      // fetch('/api/auth', {
      //   method: 'GET',
      //   credentials: 'include'
      // }).then(function(res) {
      //   return res.json()
      // }).then(function(data) {
      //   //如果有cookie，证明已经登录，无需再次登录
      //   if (data.permit) {
      //     hashHistory.push('/');
      //   }
      // })
    },
    handleClick: function(e) {
      let formBody = [];
      formBody.push(encodeURIComponent("username") + "=" + encodeURIComponent(this.refs.username.value));
      formBody.push(encodeURIComponent("password") + "=" + encodeURIComponent(this.refs.password.value));
      formBody = formBody.join("&");
      fetch(config.LOGIN_API, {
        method: 'POST',
        body: formBody,
        headers: {
          'Platform':'Web_1.0',
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }).then(function(res) {
        return res.json();
      }).then(function(data) {
        if (data.StatusCode == '0') {
          const md5 = require('md5');
          const wsToken = md5(`${data.Payload.username.toLowerCase()}_${data.Payload.token}`);
          dispatch(user_get(data.Payload));
          dispatch(conn_get(`${config.CHAT_WS}?token=${wsToken}&username=${data.Payload.username}`))
          SessionService.setToken(data.Payload.token);
          ownProps.history.push('/');
        } else{
          console.log("login error:", data)
        }
        // if (data.legal == 'yes') {
        //   dispatch(nickname_get(nickname));
        //   hashHistory.push('/');
        // } else if (data.legal == 'repeat') {
        //   alert('昵称已被占用,请重新选择昵称！');
        // } else if (data.legal == 'self login') {
        //   alert('您已进入聊天室,请勿重复进入');
        // }
      })
    }
  }
}

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginContainer
