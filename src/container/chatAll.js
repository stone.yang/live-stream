import { connect } from 'react-redux'
import ChatAll from 'components/chatall'
import {message_update, game_current} from 'action'
import SessionService from "services/session";


const mapStateToProps = (state, ownProps) => {
  return {
    user: state.user,
    msgList: state.msgList,
    count: state.count,
    games: state.games,
    game: state.game,
    empty: state.empty,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleClick: function(e) {
      //todo 登出
      SessionService.removeToken();
      ownProps.history.push('/login');

      // fetch('/api/logout', {
      //   method: 'POST',
      //   body: '',
      //   credentials: 'include'
      // }).then(function(res) {
      //   if (res.ok) {
      //     hashHistory.push('/login');
      //     dispatch(nickname_forget());
      //   }
      // });
    },
    handleSubmit: function(value, gameId) {
      //todo 傳送訊息至聊天室 {"type":"pushChatRoom","roomId":"abc","msgType":1,"msg":"hello"}
      dispatch(message_update({
        type: "pushChatRoom",
        roomId: gameId.toString(),
        msgType: 1,
        msg: value
      }));
    },
    checkLogin: function() {
      //todo 驗證
      const token = SessionService.getToken();
      if (token === null){
        ownProps.history.push('/login');
      }
      // fetch('/api/auth', {
      //   method: 'GET',
      //   credentials: 'include'
      // }).then(function(res) {
      //   return res.json()
      // }).then(function(data) {
      //   //如果没有cookie，则证明没有登录过，重定向到昵称页面
      //   if (!data.permit) {
      //     hashHistory.push('/login');
      //   } else {
      //     dispatch(nickname_get(data.nickname));
      //   }
      // })
    },
    selectGame: function(game) {
      dispatch(game_current(game));
    }
  }
}

const ChatAllContainer = connect(mapStateToProps, mapDispatchToProps)(ChatAll);
export default ChatAllContainer
