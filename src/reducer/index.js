import { combineReducers } from 'redux'
import {timestampToHH_mm} from 'helpers/date'

function user_reducer(state = '', action) {
  switch (action.type) {
    case 'USER_GET':
      return action.user;
    default:
      return state;
  }
}

function msg_reducer(state = [], action) {
  switch (action.type) {
    case 'MSG_GET':
      var newState = [];
      action.getmsg.createTime = timestampToHH_mm(action.getmsg.createTime);
      newState = [...state, action.getmsg];
      return newState;
    case 'MSG_CLEAN':
      return [];
    default:
      return state;
  }
}

function conn_reducer(state = '', action) {
  switch (action.type) {
    case 'WS_CONNECT':
      return action.host;
    default:
      return state;
  }
}

function count_reducer(state =0, action) {
  switch (action.type) {
    case 'COUNT_UPDATE':
      return action.count;
    default:
      return state;
  }
}

function games_reducer(state = [], action) {
  switch (action.type) {
    case 'GAMES_GET':
      return action.games;
    default:
      return state;
  }
}

function game_reducer(state ={}, action) {
  switch (action.type) {
    case 'GAME_CURRENT':
      return action.game;
    default:
      return state;
  }
}

function isEmpty_reducer(state =false, action) {
  switch (action.type) {
    case 'GAMES_EMPTY':
      return action.empty;
    default:
      return state;
  }
}

var reducers = combineReducers({
  user: user_reducer,
  msgList: msg_reducer,
  conn: conn_reducer,
  count: count_reducer,
  games: games_reducer,
  game: game_reducer,
  empty: isEmpty_reducer,
});

export default reducers
