import React from 'react'
import SessionService from "services/session";
import 'dist/css/main.min.css'

class Login extends React.Component {
  constructor(props) {
    super(props);
    window.onunload = () => {
      // Clear the local storage
      SessionService.removeToken();
    }
  }
  componentWillMount() {
    this.props.checkLogin();
  }
  render() {
    const handleClick = this.props.handleClick.bind(this);
    return (
        <div>
          <h2>登入</h2>
          <table>
            <tbody>
              <tr><td>帳號：</td><td><input ref='username' /></td></tr>
              <tr><td>密碼：</td><td><input ref='password' type='password' /></td></tr>
              <tr><td colSpan="2"><button style={{backgroundColor:"gray"}} onClick={handleClick}>确定</button></td></tr>
            </tbody>
          </table>
          {/*<label>帳號：</label><input ref='nick'/><br />*/}
          {/*<label>密碼：</label><input type="password" /><br />*/}
          {/*<button onClick={handleClick}>确定</button>*/}
        </div>
    )

  }
}

export default Login
