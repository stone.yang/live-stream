import React from 'react'

class TypeIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ""
        };
        this.messageSend = this.messageSend.bind(this);
        this.messageType = this.messageType.bind(this);
        this.handleDown = this.handleDown.bind(this);
        this.handleUp = this.handleUp.bind(this);
        this.handlePress = this.handlePress.bind(this);
    }
    messageType(e) {
        this.setState({
            text: e.target.value
        });
    }
    messageSend(e) {
        this.setState({
            text: ''
        });
        var value = this.refs.myInput.value;
        //只有不为空字符串的时候才执行
        if (value != 0) {
            var regLeft = /</g;
            var regRight = />/g;
            value = value.replace(regLeft, '&lt;');
            value = value.replace(regRight, '&gt;');
            value = value.replace(/\n/g, '<br/>');
            const gameId = this.props.game.matchId;
            this.props.handleSubmit(value, gameId);
        }
    }
    handleDown(e) {
        if (e.keyCode == 13) {
            if (!e.ctrlKey) {
                e.preventDefault();
                this.refs.myButton.click();
            } else {
                this.setState((prevState) => {
                    return {
                        text: prevState.text + '\n'
                    }
                });
            }
        }
    }
    handleUp(e) {
        if (e.keyCode == 13 && !e.ctrlKey) {
            e.preventDefault();
        }
        var scrollTop = this.refs.myInput.scrollHeight - this.refs.myInput.offsetHeight;
        this.refs.myInput.scrollTop = scrollTop;
    }
    handlePress(e) {
        if (e.keyCode == 13 && !e.ctrlKey) {
            e.preventDefault();
        }
    }
    render() {
        return (
            <div className="inputArea">
                <div className="stickerReview">
                    <div className="allSticker">
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                    </div>
                    <div className="stickerList">
                        <button type="button" className="left" disabled>＜</button>
                        <button type="button" className="right">＞</button>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                        <img src="img/test-sticker.png" alt=""/>
                    </div>
                </div>

                <textarea name="textField" id="textField" cols="25" rows="4" placeholder="输入讯息" ref="myInput" onKeyUp={this.handleUp} onChange={this.messageType} onKeyDown={this.handleDown} value={this.state.text} onKeyPress={this.handlePress}></textarea>
                <div className="tools">

                    {/*<a href="#" className="emoji">*/}
                    {/*    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"*/}
                    {/*         viewBox="0 0 20 20">*/}
                    {/*        <path*/}
                    {/*            d="M12.99,3A10,10,0,1,0,23,13,10,10,0,0,0,12.99,3ZM13,21a8,8,0,1,1,8-8A8,8,0,0,1,13,21Zm3.5-9A1.5,1.5,0,1,0,15,10.5,1.5,1.5,0,0,0,16.5,12Zm-7,0A1.5,1.5,0,1,0,8,10.5,1.5,1.5,0,0,0,9.5,12ZM13,18.5A5.5,5.5,0,0,0,18.11,15H7.89A5.5,5.5,0,0,0,13,18.5Z"*/}
                    {/*            transform="translate(-3 -3)"/>*/}
                    {/*    </svg>*/}
                    {/*</a>*/}

                    {/*<a href="#" className="fileUpload">*/}
                    {/*    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="20"*/}
                    {/*         viewBox="0 0 17 20">*/}
                    {/*        <path*/}
                    {/*            d="M18.818,6.445a2.6,2.6,0,0,1,.531.848,2.594,2.594,0,0,1,.221.982V21.132a1.038,1.038,0,0,1-.31.759,1.02,1.02,0,0,1-.753.313H3.633a1.02,1.02,0,0,1-.753-.313,1.038,1.038,0,0,1-.31-.759V3.275a1.038,1.038,0,0,1,.31-.759A1.02,1.02,0,0,1,3.633,2.2H13.55a2.537,2.537,0,0,1,.974.223,2.573,2.573,0,0,1,.841.536ZM13.9,3.721v4.2h4.161a1.222,1.222,0,0,0-.243-.458L14.358,3.967a1.206,1.206,0,0,0-.454-.246Zm4.25,17.054V9.346h-4.6a1.02,1.02,0,0,1-.753-.312,1.038,1.038,0,0,1-.31-.759V3.632h-8.5V20.775H18.154Zm-1.417-5v3.571H5.4V17.2l2.125-2.143,1.417,1.429L13.2,12.2ZM7.529,13.632a2.041,2.041,0,0,1-1.505-.625,2.168,2.168,0,0,1,0-3.036,2.125,2.125,0,0,1,3.01,0,2.168,2.168,0,0,1,0,3.036A2.041,2.041,0,0,1,7.529,13.632Z"*/}
                    {/*            transform="translate(-2.571 -2.203)"/>*/}
                    {/*    </svg>*/}
                    {/*</a>*/}

                    {/*<span>0/20</span>*/}

                    <a href="#" id="send" ref="myButton" onClick={this.messageSend}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20.002" height="20"
                             viewBox="0 0 20.002 20">
                            <path
                                d="M23.874,4.548,4.751,12.883a.438.438,0,0,0,.016.8L9.939,16.6a.834.834,0,0,0,.953-.094l10.2-8.793c.068-.057.229-.167.292-.1s-.036.224-.094.292l-8.825,9.939a.831.831,0,0,0-.083.995l3.381,5.423a.44.44,0,0,0,.792-.01L24.463,5.126A.438.438,0,0,0,23.874,4.548Z"
                                transform="translate(-4.503 -4.503)"/>
                        </svg>
                    </a>
                </div>
            </div>
        )
    }
}

export default TypeIn