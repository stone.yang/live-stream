import React from 'react'
import ReactHlsPlayer from 'react-hls-player';

class Player extends React.Component {
    constructor(props) {
        super(props);
        const {game} = this.props;
        this.state = {
            game: game
        };
    }

    shouldComponentUpdate() {
        const {game} = this.props;
        if(this.state.game.matchId != game.matchId || this.state.game.liveStreamingHttps != game.liveStreamingHttps){
            this.setState({
                game: game
            });
            return true;
        }
        return false;
    }

    render() {
        const {game} = this.props;
        let view;
        if (game.liveStreamingHttps) {
            view = <>
                    <div className="mark">
                        <span className="badge live">LIVE</span>
                        <a href="" className="closeChat">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">
                                <path d="M11.5,17.5h6v-6h4l-7-7-7,7h4Zm-4,2h14v2H7.5Z"
                                      transform="translate(21.5 -7.5) rotate(90)" />
                            </svg>
                        </a>
                    </div>
                {/* test hls https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8 */}
                    <ReactHlsPlayer
                    src={game.liveStreamingHttps}
                    autoPlay={true}
                    controls={true}
                    width="100%"
                    height="auto"
                    />
                    </>;
        } else {
            view = <iframe  width="100%" height="630" src={this.props.game.animation} frameBorder="0" allowFullScreen></iframe>;
        }

        return (
            <>
                {view}
            </>
        )
    }
}

export default Player