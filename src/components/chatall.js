import React from 'react'
import TypeIn from 'components/typein'
import MsgShow from 'components/msgshow'
// import Nav from '../nav/index.js'
import GameList from 'components/gamelist'
import Game from 'components/game'
import SessionService from "services/session";
import 'dist/css/main.min.css'

class ChatAll extends React.Component {
    constructor(props) {
        super(props);
        window.onunload = () => {
            // Clear the local storage
            SessionService.removeToken();
        }
    }

    componentWillMount() {
        this.props.checkLogin();
    }

    render() {
        const {handleClick, msgList, user, handleSubmit, count, games, game, selectGame, empty} = this.props;
        // if (nickName == '') {
        //     return null;
        // } else {
        //     return (
        //     )
        // }
        return (
            <main>
                <GameList selectGame={selectGame} games={games} game={game}/>
                <Game game={game} count={count} empty={empty} />
                <div id="chatRoom">
                    <MsgShow msgList={msgList} handleClick={handleClick} user={user} />
                    {this.props.game.matchId && (
                        <TypeIn handleSubmit={handleSubmit} game={game}/>
                    )}
                </div>
            </main>
        )
    }
}

export default ChatAll
