Date.prototype.format = function (fmt) {
    const o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小時
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (const k in o)
        if (new RegExp("(" +  k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" +  o[k]).substr(("" + o[k]).length)));
    return fmt;
}

// 賽事列表
export function timestampToMM_dd_hh_mm(t){
    const d = new Date(t * 1000);
    return d.format("MM-dd hh:mm");
}

// 聊天時間
export function timestampToHH_mm(t){
    const d = new Date(t * 1000);
    return d.format("hh:mm");
}

// 目前賽事
export function timestampToDateTime(t){
    if (t==null){
        return "";
    }
    const d = new Date(t * 1000);
    return d.format("yyyy-MM-dd hh:mm:ss");
}