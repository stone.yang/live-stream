import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import { history } from 'helpers/history';
import Demo from 'components/demo';
import LoginContainer from 'container/login.js'
import ChatAllContainer from "container/chatAll";

function App() {
    return (
        <Router history={history}>
            <Switch>
                <Route exact={true} path='/' component={ChatAllContainer}/>
                <Route exact={true} path='/login' component={LoginContainer}/>
                <Route exact={true} path='/demo' component={Demo}/>
            </Switch>
        </Router>
    );
}

export default App;
