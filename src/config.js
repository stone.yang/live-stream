// https://tstgen.tg7777.net/relayApi/api/members/login
// https://tstgendev.tg7777.net/relayApi/api/games/leagues/all/live_streaming_list
// wss://2021eu.k33uc.com/ws?token=${wsToken}&username=${data.Payload.username}

const dev = {
    LOGIN_API: "https://tstgen.tg7777.net/relayApi/api/members/login",
    GAMES_API: "https://tstgendev.tg7777.net/relayApi/api/games/leagues/all/live_streaming_list",
    CHAT_WS: "wss://2021eu.k33uc.com/ws",
    DEV_ENV: true
};

const prod = {
    LOGIN_API: "https://tstgen.tg7777.net/relayApi/api/members/login",
    GAMES_API: "https://tstgendev.tg7777.net/relayApi/api/games/leagues/45/live_streaming_list",
    CHAT_WS: "wss://2021eu.k33uc.com/ws",
    DEV_ENV: false
};

const config = {
    // Add common config values here
    MAX_ATTACHMENT_SIZE: 5000000,
    // Default to dev if not set
    ...(process.env.REACT_APP_STAGE === "prod" ? prod : dev),
};

export default config;