import { message_get, count_update, games_get, game_current, message_clean, games_empty } from '../action'
import GameService from "../services/game";

function createSocketMiddleware() {
  let socket = null;

  const onOpen = store => (event) => {
    //todo done 訂閱房間
    (async () => {
      const data = await GameService.getGames();
      // todo 未來如果提供訂閱多個房間再使用
      // for (let i = 0; i < data.length; i++) {
      //   store.dispatch(game_ws(data[i].matchId.toString()));
      // }
      if (data.length > 0){
        store.dispatch(games_get(data));
        store.dispatch(game_current(data[0]));
      } else {
        store.dispatch(games_empty(true));
      }
    })();
  };

  const onMessage = store => (event) => {
    const payload = JSON.parse(event.data);
    switch (payload.command) {
      case 8100:
        // todo 接收訊息 新訊息或傳送訊息成功 message_get
        console.log('接收訊息:', payload);
        store.dispatch(message_get(payload.message));
        // {
        //   "command": 8100,
        //   "roomId": "abc",
        //   "message": {
        //       "msgId": 1399387842190254000,
        //       "msgType": 1,
        //       "msgText": "你好",
        //       "fromUser": {
        //          "chatUserName": "bg4h479taywyy",
        //         "nickName": "高雄發大材"
        //        },
        //   "createTime": 1622475035
        // }
        break;
      case 8103:
        // todo 當前room 人數推送 {"command":8103,"roomId":"abc","userCnt":1}
        if(payload.userCnt){
          store.dispatch(count_update(payload.userCnt));
        }
        break;
      case 8101:
        //   訂閱成功
        // todo {"command":8101,"roomId":"abc","statusCode":0,"chatUserName":"bg4h479taywyy"}
        console.log('訂閱成功', payload);
        break;
        // case 'update_game_players':
        //   store.dispatch(updateGame(payload.game));
        //   break;
        // case 'update_timer':
        //   store.dispatch(updateTimer(payload.time));
        //   break;
        // case 'update_game_player':
        //   console.log(payload);
        //   store.dispatch(updateGamePlayer(payload.current_player));
        //   break;
      default:
        console.log(payload);
        break;
    }
  };

  return store => next => action => {
    // if (action.type == 'MSG_UPDATE') {
    //   socket.emit('msg from client', action.msg);
    // } else if (action.type == 'NICKNAME_GET') {
    //   socket.emit('guest come', action.nickName);
    // } else if (action.type == 'NICKNAME_FORGET') {
    //   socket.emit('guest leave', store.getState().nickName);
    // }
    switch (action.type) {
      case 'WS_CONNECT':
        if (socket !== null) {
          socket.close();
        }
        socket = new WebSocket(action.host);
        socket.onmessage = onMessage(store);
        socket.onopen = onOpen(store);
        // console.log('begin to mount');
        // socket.on('guest update', function(data) {
        //   next(guest_update(data));
        // });
        // socket.on('msg from server', function(data) {
        //   next(message_update(data));
        // });
        // socket.on('self logout', function() {
        //   window.location.reload();
        // });

        // setInterval(function() {
        //   socket.emit('heart beat');
        // }, 10000);
        break;
      case 'WS_DISCONNECT':
        if (socket !== null) {
          socket.close();
        }
        socket = null;
        break;
      case 'GAME_CURRENT':
        //TODO done 訂閱推送內容
        store.dispatch(count_update(0));
        store.dispatch(message_clean());
        if (action.game.matchId){
          console.log('訂閱房間:', JSON.stringify({ type: 'subChatRoom', roomId: action.game.matchId.toString() }));
          socket.send(JSON.stringify({ type: 'subChatRoom', roomId: action.game.matchId.toString() }));
        }
        break;
      case 'MSG_UPDATE':
        //{"type":"pushChatRoom","roomId":"abc","msgType":1,"msg":"hello"}
        socket.send(JSON.stringify(action.msg));
        console.log('傳送訊息:', JSON.stringify(action.msg));
        break;
      // case 'NICKNAME_GET':
      //   socket.send(JSON.stringify({ command: 'NEW_MESSAGE', message: action.msg }));
      //   break;
      // case 'NICKNAME_FORGET':
      //   socket.send(JSON.stringify({ command: 'NEW_MESSAGE', message: action.msg }));
      //   break;
    }
    return next(action);
  }
}

export default createSocketMiddleware
